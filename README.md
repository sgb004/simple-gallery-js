# Simple Gallery JS

A simple gallery using Tiny Slider

## Requirements

This package is thinking to use with webpack.

## Dependencies

```
tiny-slider
custom-filters-js
```

Check [package.json](./package.json) for more information.

## Installation

```npm
npm i simple-gallery-for-js
```

## Usage

Add html:

```html
<div id="simple-gallery-container">
	<a href="img-01.webp" class="simple-gallery-item" target="_blank"
		><img src="img-01-thumbnail.jpg"
	/></a>
	<a href="img-02.webp" class="simple-gallery-item" target="_blank"
		><img src="img-02-thumbnail.jpg"
	/></a>
	<a href="img-03.webp" class="simple-gallery-item" target="_blank"
		><img src="img-03-thumbnail.jpg"
	/></a>
</div>
```

In your main .js add simple-gallery-for-js, simple-gallery.css and

```javascript
import 'simple-gallery-for-js/css/simple-gallery.css';
import { SimpleGallery } from 'simple-gallery-for-js';

document.addEventListener('DOMContentLoaded', function () {
	const simpleGalleryContainer = new SimpleGallery('#simple-gallery-container');
});
```

## Class SimpleGallery

### Properties

#### gallerySlider

This contains a slider created with Tiny Slider. The slider is built the first time the gallery is opened.

### Methods

#### constructor(element)

This requires the class name or id of the gallery.

#### open(index)

This opens the gallery, it requires the index of the image with which the gallery will be opened.

#### close

This closes the gallery.

### Filters

You can use filters in Simple Gallery.

To add a filter you can use:

<!-- prettier-ignore-start -->

```javascript
myGallery.addFilterListener('filter_name', function (p1) {
	console.log(p1);
	return p1;
});
```
<!-- prettier-ignore-end -->

The available filters are:

#### index

Before the gallery is open, you can change the index of the image to open.

<!-- prettier-ignore-start -->

```javascript
myGallery.addFilterListener('index', function (index) {
	console.log(index);
	return index;
});
```
<!-- prettier-ignore-end -->

#### tinySlider

You can change settings of the Tiny Slider

<!-- prettier-ignore-start -->

```javascript
myGallery.addFilterListener('tinySlider', function (tinySlider) {
	console.log(tinySlider);
	return tinySlider;
});
```
<!-- prettier-ignore-end -->

### Events

The available events are:

#### beforeOpen

<!-- prettier-ignore-start -->
```javascript
myGallery.addEventListener('beforeOpen', function (e) {});
```
<!-- prettier-ignore-end -->

#### afterOpen

<!-- prettier-ignore-start -->
```javascript
myGallery.addEventListener('afterOpen', function (e) {});
```
<!-- prettier-ignore-end -->

#### beforeClose

<!-- prettier-ignore-start -->
```javascript
myGallery.addEventListener('beforeClose', function (e) {});
```
<!-- prettier-ignore-end -->

#### afterClose

<!-- prettier-ignore-start -->
```javascript
myGallery.addEventListener('afterClose', function (e) {});
```
<!-- prettier-ignore-end -->
