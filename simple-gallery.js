/**
 * @name: SimpleGallery
 * @author @sgb004
 * @version 0.1.0
 */

import { tns } from 'tiny-slider/src/tiny-slider';
import { CustomFilters } from 'custom-filters-js';
export class SimpleGallery {
	#__customFilters;
	gallerySlider;

	constructor(container) {
		this.container = document.querySelector(container);
		this.sliderLoaded = false;

		let items = this.container.querySelectorAll('.gallery-item');
		let i;
		for (i = 0; i < items.length; i++) {
			items[i].setAttribute('data-index', i);
			items[i].addEventListener('click', this.itemClick.bind(this));
		}

		this.#__customFilters = new CustomFilters(this);
	}

	itemClick(e) {
		e.preventDefault();
		this.open(e.target.parentNode.getAttribute('data-index'));
	}

	open(i) {
		i = this.dispatchFilter('index', i);
		this.dispatchEvent('beforeOpen', { index: i });
		this.isSliderLoaded();
		this.gallerySliderContainer.classList.add('open');
		this.gallerySlider.goTo(i);
		this.dispatchEvent('afterOpen', { index: i });
	}

	close() {
		this.dispatchEvent('beforeClose');
		this.gallerySliderContainer.classList.remove('open');
		this.dispatchEvent('afterClose');
	}

	closeBtnClick(e) {
		e.preventDefault();
		this.close();
	}

	isSliderLoaded() {
		if (!this.sliderLoaded) {
			this.makeSlider();
			this.sliderLoaded = true;
		}
	}

	makeSlider() {
		const items = this.container.querySelectorAll('.gallery-item');
		let sliderItems =
			'<a href="#" class="simple-gallery-slider-close-btn">Close</a><div class="simple-gallery-slider-items">';
		let i;
		this.gallerySliderContainer = document.createElement('div');
		this.gallerySliderContainer.classList.add(
			'simple-gallery-slider-container'
		);
		for (i = 0; i < items.length; i++) {
			sliderItems +=
				'<div class="item"><img src="' + items[i].href + '"></div>';
		}
		sliderItems += '</div>';
		this.gallerySliderContainer.innerHTML = sliderItems;
		this.gallerySliderContainer
			.querySelector('.simple-gallery-slider-close-btn')
			.addEventListener('click', this.closeBtnClick.bind(this));
		this.container.appendChild(this.gallerySliderContainer);

		let tnsSettings = this.dispatchFilter('tinySlider', {
			container: this.gallerySliderContainer.querySelector(
				'.simple-gallery-slider-items'
			),
			controls: true,
			rewind: false,
			speed: 1000,
			autoplay: true,
			autoplayHoverPause: true,
			autoplayTimeout: 3500,
		});

		this.gallerySlider = tns(tnsSettings);
	}

	dispatchEvent(event, detail) {
		let e = new CustomEvent(event, { detail });
		this.container.dispatchEvent(e);
	}

	addEventListener(type, listener, useCapture) {
		this.container.addEventListener(type, listener.bind(this), useCapture);
	}

	dispatchFilter() {
		return this.#__customFilters.dispatchFilter(arguments);
	}

	addFilterListener(filter, fn) {
		this.#__customFilters.addFilterListener(filter, fn);
	}
}
